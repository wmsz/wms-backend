package com.space.wms.backend.exception;

import org.apache.catalina.User;

public class UserException extends BaseException {
    public UserException(String code) {
        super("user" + code);
    }

    public static UserException emailNull(){
        return new UserException("register.email.null");
    }
}
