package com.space.wms.backend.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class AuthenticationController {

    @GetMapping("/login")
    public String doLogin()
    {
        return "Hello login";
    }

    @GetMapping("/register")
    public String doRegister()
    {
        return "Hello Registers";
    }
}
