# Use official base image of Java Runtime
FROM openjdk:16-alpine

# Who is MAINTAINER
MAINTAINER Pattaranon Plyduang, pattaranon.ply@gmail.com

# Copy jar file to container
COPY ./target/wms-api.jar wms-api.jar

# Set port
EXPOSE 9081

# Run the JAR file
ENTRYPOINT ["java","-jar","/wms-api.jar"]


